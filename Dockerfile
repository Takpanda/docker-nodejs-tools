FROM alpine:3.14.0
LABEL maintainer="tak.aoki.jp@gmail.com"
 
ENV LANG=ja_JP.UTF-8 JP_PORT=8888 JP_USER=gitlab
 
#EXPOSE ${JP_PORT}
SHELL ["/bin/ash", "-eo", "pipefail", "-c"]

RUN apk update && apk upgrade && apk add --no-cache \
        curl \
        git \
        cloc && \
        curl -fsSL https://deb.nodesource.com/setup_18.x | ash - && \
        apk add --no-cache nodejs npm

RUN curl -L -o nkf.apk https://github.com/tom-tan/alpine-pkg-nkf/releases/download/2.1.4-r0/nkf-2.1.4-r0.apk && \
    apk add --allow-untrusted --no-cache nkf.apk && \
    rm nkf.apk

RUN npm init -g -y && \
    npm install -g iconv-lite && \
    npm install -g commander && \
    npm install -g ejs && \
    npm install -g fast-xml-parser && \
    npm install -g node-fetch@2.6.7 && \
    npm install -g eslint && \
    npm install -g eslint-plugin-compat && \
    npm install -g eslint-formatter-table && \
    npm install -g eslint-formatter-rdjson && \
    npm install -g stylelint && \
    npm install -g stylelint-config-standard && \
    npm install -g stylelint-html-formatter && \
    npm install -g stylelint-no-unsupported-browser-features && \
    npm install -g uglify-js && \
    npm install -g browserslist && \
    adduser ${JP_USER} --home /home/${JP_USER} --shell /bin/bash -D

#RUN npx browserslist@latest --update-db

 
