# Node.js with Tools

The latest tag is currently:

- Node : 14.17.1
- npm : 7.x
- git : 2.23.0
- curl : latest

```
npm
├── commander@7.2.0
├── eslint-plugin-compat@3.9.0
├── eslint@7.29.0
├── iconv-lite@0.6.3
├── mocha-junit-reporter@2.0.0
├── mocha@9.0.1
├── nyc@15.1.0
├── uglify-js@3.13.9
└── vsce@1.93.0
```

### Use as base image
```dockerfile
FROM registry.gitlab.com/takpanda/docker-nodejs-tools:latest
```
